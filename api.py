# Dependencies
#from os import sysconf_names
from flask import Flask, request, jsonify
import joblib
import traceback
import pandas as pd
import numpy as np
import sys
import matplotlib.pyplot as plt
from pandas import DataFrame
import statistics


# Your API definition
app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():
    if log_model:
        try:
            file = request.files['file']
            
            json_ = file.read()            
            splitted_lines = json_.splitlines()

            preprocessed_data = preprocessing(splitted_lines)

            prediction = list(log_model.predict(preprocessed_data))
            
            final_mode_prediction = statistics.mode(prediction)

            return jsonify({'prediction': str(final_mode_prediction)})

        except:

            return jsonify({'trace': traceback.format_exc()})
    else:
        print ('Train the model first')
        return ('No model here to use')


def preprocessing(fileContent):
    acc=list()
    gyro=list()
    prox=list()
    rot=list()

    s = fileContent

    for i in range(len(s)):
        print("i is ", i)
        decoded_val = s[i].decode("utf-8")
        
        acc_start = decoded_val.find("Acc:")      
        acc_end = decoded_val.find("Gyro:")        
        acc_substring = decoded_val[acc_start:acc_end][4:] 
            
        gyro_start = decoded_val.find("Gyro:")      
        gyro_end = decoded_val.find("Mag:")
        gyro_substring = decoded_val[gyro_start:gyro_end]
            
        prox_start = decoded_val.find("prox:")      
        prox_substring = decoded_val[prox_start:]
            
            
        rot_start = decoded_val.find("rot:")      
        rot_end = decoded_val.find("Mot:")
        rot_substring = decoded_val[rot_start:rot_end]
            
            
        
            
        if(len(gyro_substring)>10 and len(prox_substring)>10 and len(rot_substring)>10 ):
                numberList = []
                gyro_substring = decoded_val[gyro_start:gyro_end][5:].strip()
                numberList = [float(i) for i in gyro_substring[1:-1].split(',')]
                average_gyro=np.mean(numberList)
                
                
                prox_substring = decoded_val[prox_start:][5:].strip()
                numberList = [float(i) for i in prox_substring[1:-1].split(',')]
                average_prox=np.mean(numberList)
                
                rot_substring = decoded_val[rot_start:rot_end][4:].strip()
                print(rot_substring)
                numberList = [float(i) for i in rot_substring[1:-1].split(',')]
                average_rot=np.mean(numberList)
                
                
                
                numberList = [float(i) for i in acc_substring[1:-1].split(',')]
                average_acc=np.mean(numberList)
                
                acc.append(average_acc)
                gyro.append(average_gyro)
                prox.append(average_prox)
                rot.append(average_rot)
                


    final_training_data = DataFrame(np.vstack((acc,gyro, prox, rot)).T)
    final_training_data.columns=['Acc','Gyro','Prox','Rot']
    
    return final_training_data



if __name__ == '__main__':
    
    port=12345

    log_model = joblib.load("model.pkl") # Load "model.pkl"
    print ('Model loaded')
    model_columns = joblib.load("model_columns.pkl") # Load "model_columns.pkl"
    print ('Model columns loaded')

    app.run(host='0.0.0.0', port=12345, debug=False)