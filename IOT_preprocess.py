# -*- coding: utf-8 -*-
"""
Created on Sat Nov 13 20:18:54 2021

@author: bthapaliya1
"""


import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pandas import DataFrame


acc=list()
gyro=list()
prox=list()
rot=list()
with open('Game_sensordata.txt') as f:
    s = f.readlines()
    for i in range(len(s)):
        acc_start = s[i].find("Acc:")      
        acc_end = s[i].find("Gyro:")        
        acc_substring = s[i][acc_start:acc_end][4:] 
        
        gyro_start = s[i].find("Gyro:")      
        gyro_end = s[i].find("Mag:")
        gyro_substring = s[i][gyro_start:gyro_end]
        
        prox_start = s[i].find("prox:")      
        prox_substring = s[i][prox_start:]
        
        
        rot_start = s[i].find("rot:")      
        rot_end = s[i].find("Mot:")
        rot_substring = s[i][rot_start:rot_end]
        
        
       
        
        if(len(gyro_substring)>10 and len(prox_substring)>10 and len(rot_substring)>10 ):
            numberList = []
            gyro_substring = s[i][gyro_start:gyro_end][5:].strip()
            numberList = [float(i) for i in gyro_substring[1:-1].split(',')]
            average_gyro=np.mean(numberList)
            
            
            prox_substring = s[i][prox_start:][5:].strip()
            numberList = [float(i) for i in prox_substring[1:-1].split(',')]
            average_prox=np.mean(numberList)
            
            rot_substring = s[i][rot_start:rot_end][4:].strip()
            print(rot_substring)
            numberList = [float(i) for i in rot_substring[1:-1].split(',')]
            average_rot=np.mean(numberList)
            
            
            
            numberList = [float(i) for i in acc_substring[1:-1].split(',')]
            average_acc=np.mean(numberList)
            
            acc.append(average_acc)
            gyro.append(average_gyro)
            prox.append(average_prox)
            rot.append(average_rot)
            


final_training_data = DataFrame(np.vstack((acc,gyro, prox, rot)).T)
final_training_data.columns=['Acc','Gyro','Prox','Rot']
final_training_data["Label"]='Game'
#final_training_data.to_csv("Game_training_data.csv")
            
